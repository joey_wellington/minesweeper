package sweeper;

public class Game {

    private Bomb bomb;
    private Flag flag;
    private GameState state;

    public GameState getState() {
        return state;
    }


    public Game (int cols, int rows, int bombs) {
        Ranges.setSize(new Coordinate(cols, rows));
        bomb = new Bomb(bombs);
        flag = new Flag();
    }

    public void start() {
        bomb.start();
        flag.start();
        state = GameState.PLAYED;
    }

    private void checkWinner() {
        if (state == GameState.PLAYED) {
            if (flag.getCountOfClosedBoxes() == bomb.getTotalBombs()) {
                state = GameState.WINNER;
            }
        }
    }

    public Box getBox(Coordinate coordinate) {
        if (flag.get(coordinate) == Box.OPENED) {
            return bomb.get(coordinate);
        }
        return flag.get(coordinate);
    }

    public void pressLeftButton(Coordinate coordinate) {
        if (gameOver()) {
            return;
        }
        openBox(coordinate);
        checkWinner();
    }

    private void openBox(Coordinate coordinate) {
        switch (flag.get(coordinate)) {
            case OPENED: setOpenedToClosedBoxesAroundNumber(coordinate); return;
            case FLAGED: return;
            case CLOSED:
                switch (bomb.get(coordinate)) {
                    case ZERO: openBoxesAround(coordinate); return;
                    case BOMB: openBombes(coordinate); return;
                    default:   flag.setOpenedToBox(coordinate);
                }
        }
    }

    private void setOpenedToClosedBoxesAroundNumber(Coordinate coordinate) {
        if (bomb.get(coordinate) != Box.BOMB) {
            if (flag.getCountOfFlagedBoxesAround(coordinate) == bomb.get(coordinate).getNumber()) {
                for (Coordinate around : Ranges.getCoordsAround(coordinate)) {
                    if (flag.get(around) == Box.CLOSED) {
                        openBox(around);
                    }
                }
            }
        }
    }

    private void openBombes(Coordinate bombed) {
        state = GameState.BOMBED;
        flag.setBombedToBox(bombed);
        for (Coordinate coordinate : Ranges.getAllCords()) {
            if (bomb.get(coordinate) == Box.BOMBED) {
                flag.setOpenedToClosedBombBox(coordinate);
            } else {
                flag.setNoBombToFlagedSafeBox(coordinate);
            }
        }
    }

    private void openBoxesAround(Coordinate coordinate) {
        flag.setOpenedToBox(coordinate);
        for (Coordinate around : Ranges.getCoordsAround(coordinate)) {
            openBox(around);
        }
    }

    public void pressRightButton(Coordinate coordinate) {
        if (gameOver()) {
            return;
        }
        flag.toggleFlagedToBox(coordinate);
    }

    private boolean gameOver() {
        if (state == GameState.PLAYED) {
            return false;
        }
        start();
        return true;
    }
}
